import { defineStore } from 'pinia'

export const userInfo = defineStore('user-store', {
  state: () => {
    return {
      isAuth: false,
      name: '',
      login: '',
      viewCount: null
    }
  },

  getters: {
    userName(state) {
      return state.name;
    }
  },

  actions: {
    auth(data) {
      this.isAuth = true;
      this.name = data.name;
      this.login = data.login;
      this.viewCount = 0
    },
    logout() {
      this.isAuth = false;
      localStorage.removeItem('user');
      this.name = ''
      this.login = ''
    }
  }
})