import http from "../../http-common";
import { userInfo } from '../stores/user.js';
import authHeader from './authHeader.js'

class dataService {

  signin(data) {
    return http.post("auth/signin", data);
    /* {
    "name": "test1",
    "login": "newlogin",
    "password": "123"
  } */
  }

  signup(data) {
    return http.post("auth/signup", data);
    /* "login": "newlogin",
	  "password": "123" */
  }

  getPublicContent() {
    return http.get("content/getData");
  }

  getPrivateContent() {
    return http.get("content/getPrivateData", { headers: authHeader() });
  }

  uploadFeedbackForm(data) {
    return http.post("feedback/postData", data, { headers:authHeader() })
  }
  
  refreshTokenRequest(currentToken) {
    return http.post("auth/refreshtoken", currentToken)
  }
}

export default new dataService();