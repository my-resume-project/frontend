import { createRouter } from 'vue-router'
import { userInfo } from './stores/user.js';
import Homepage from './pages/HomePage.vue'
import AboutPage from './pages/AboutPage.vue'
import SignIn from './pages/SignIn.vue'
import DetailsPage from './pages/DetailsPage.vue'
import ActivePage from './pages/ActivePage.vue'
import MyVideo from './pages/MyVideo.vue'

const routes = [
  {
    path: '/',
    component: Homepage
  },
  {
    path: '/about',
    component: AboutPage
  },
  {
    path: '/auth',
    component: SignIn,
    beforeEnter: (to, from) => {
      const clientInfo = userInfo()
      if (clientInfo.isAuth) {
        return { path: '/' }
      }
    },
  },
  {
    path: '/details',
    component: DetailsPage,
    beforeEnter: (to, from) => {
      const clientInfo = userInfo()
      if (!clientInfo.isAuth) {
        return { path: '/auth' }
      }
    },
  },
  {
    path: '/MyVideo',
    component: MyVideo
  },
  {
    path: '/game',
    component: ActivePage
  },
]

export default function (history) {
  return createRouter({
    history,
    routes
  })
}
