import axios from "axios";

export default axios.create({
  baseURL: "https://express-38zs.onrender.com/api",
  headers: {
    "Content-type": "application/json"
  }
});